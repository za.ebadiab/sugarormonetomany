package com.example.sugarormonetomany;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.sugarormonetomany.model.Author;
import com.example.sugarormonetomany.model.Book;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("TAG", "before creating author2, author1 has book1,book2 ");

        Author author1 = new Author("author1");
        author1.save();


        Book book1 = new Book("book1");
        book1.author = author1;
        book1.save();

        Book book2 = new Book("book2");
        book2.author = author1;
        book2.save();

        List<Book> b = Book.listAll(Book.class);

        // result:  author = author1      book = book1 , book2
        for (int i = 0; i < b.size(); i++) {
            Log.d("TAG", "book: " + b.get(i).getName() + "  author= " + b.get(i).author.getName());
        }


        //result:   author2 with 3 books (book1 , book2 , book3)
        Author author2 = new Author("author2");
        author2.save();


        Book book3 = new Book("book3");
        book3.author = author2;
        book3.save();

        book2.author = author2;
        book2.save();

        book1.author = author2;
        book1.save();

        Log.d("TAG", "after creating author2 with book1,book2,book3 because of one-to-many, author2 replaced to author1 and now author1 has no books  " );

         b = Book.listAll(Book.class);

        for (int i = 0; i < b.size(); i++) {
            Log.d("TAG", "book: " + b.get(i).getName() + "  author= " + b.get(i).author.getName());
        }

        // List of authors
        Log.d("TAG", "List of authors: ");
        List<Author> authorList = Author.listAll(Author.class);
        for (int i = 0; i < authorList.size(); i++) {
            Log.d("TAG", "Author= " + authorList.get(i).getName());
        }


        // List of books
        Log.d("TAG", "List of books: ");
        List<Book> bookList = Book.listAll(Book.class);
        for (int i = 0; i < bookList.size(); i++) {
            Log.d("TAG", "Book= " + bookList.get(i).getName());
        }

    }
}