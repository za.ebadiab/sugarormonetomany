package com.example.sugarormonetomany.model;

import com.orm.SugarRecord;

import java.util.List;

public class Author extends SugarRecord {

    String name;
    public Author() {
    }

    public Author(String name) {
        this.name = name;
    }

    // one (author) - many (books)
    public List<Book> getBooks(){
//   error:  List<Book> books = Book.find(Book.class, "author = ?", new String{author.getId()});
        return Book.find(Book.class, "author = ?", String.valueOf(this.getId()));

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
