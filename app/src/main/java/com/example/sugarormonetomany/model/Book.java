package com.example.sugarormonetomany.model;

import com.orm.SugarRecord;

public class Book extends SugarRecord {
    String name;
    // one (author) - many (books)
    public Author author;

    public Book() {
    }

    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
